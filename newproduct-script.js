//This will result in the handler not being called if 
//the DOM is already loaded at the time.
window.addEventListener("DOMContentLoaded", function() {
        
        // Button to return to main page
        document.getElementById("myButton").onclick = function (e) {
          e.preventDefault();
            location.href = "/";
        };

});


$(document).ready(function () {

  var myValidateObj = {
    rules: {
      sku: {
        required: true,
        number: true
        },
       name: {
        required: true,
        },
      price: {
        required: true,
        number: true
        },
      weight: {
        required: true,
        number: true
        },
      height:{
        required: true,
        number: true
        },
      width:{
        required: true,
        number: true
        },
      length:{
        required: true,
        number: true
        }
    }
  };
  
$('#productType').change(function () {
      var id = $(this)
        .children(":selected")
        .prop("id");

 

      // Clear child elements added before
      $("#SwitcherBoxes").empty();
      
      jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
      });     
      
              // INITIALIZE plugin on the traditional form
      var validate1 = $('#product_form').validate(myValidateObj);

// Condition of type switcher position
     if (id === "Book") {

      //Create a div
      var div = $("<div>Weight:<br></div>");
  
      //Append the div into "SwitcherBoxes" div
      $("#SwitcherBoxes").append(div);
      
      var BookField = '<form id="BookField"><input type="text" name="weight" id="weight"/></form>'

      $("#SwitcherBoxes").append(BookField);



      // INITIALIZE plugin on the new form
      var validate = $('#BookField').validate(myValidateObj);
  
      // Condition of type switcher position
    
  } else if (id === "Furniture") {
 
      //Create a div
      var div = $("<div></div>");
      //Append the div into "SwitcherBoxes" div
      $("#SwitcherBoxes").append(div);
    
      //Create a div
      $("<br><text>Height:</text>").appendTo(div);

      var furnitureField1 = '<form id="furnitureField1"><input type="text" name="height" id="height"/></form>'
      
      //Insert furnitureField1 field into the div recently created
      $(div).append(furnitureField1);

      //Create a div
      $("<br><text>Width:</text>").appendTo(div);

      var furnitureField2 = '<form id="furnitureField2"><input type="text" name="width" id="widtht"/></form>'
      
      //Insert furnitureField2 field into the div recently created
      $(div).append(furnitureField2);

      //Create a div
      $("<br><text>Length:</text>").appendTo(div);

      var furnitureField3 = '<form id="furnitureField3"><input type="text" name="length" id="length"/></form>'
      
      //Insert furnitureField3 field into the div recently created
      $(div).append(furnitureField3);


      //Append the div into "SwitcherBoxes" div
      $("#SwitcherBoxes").append(div);

      $(
        "<br><text>Please provide the dimensions in HxWxL format</text>"
      ).appendTo(div);
      // INITIALIZE plugin on the new form
      var validate = $('#furnitureField1').validate(myValidateObj);
      var validate = $('#furnitureField2').validate(myValidateObj);
      var validate = $('#furnitureField3').validate(myValidateObj);
    
// Condition of type switcher position
  } else if (id === "DVD") {
    // Clear child elements added before
    $("#SwitcherBoxes").empty();

    //Create a div
    var div = $("<div>Size:<br></div>");

    //Append the div into "SwitcherBoxes" div
    $("#SwitcherBoxes").append(div);

    //Insert textbox into the div recently created
    $("<input />", {
      type: "text",
      id: "size",
      value: "",
      name: "size"
    }).appendTo(div);
    $("<br><text>Please provide the size attribute in MB</text>").appendTo(div);
  }
});

});


//This will result in the handler not being called if 
//the DOM is already loaded at the time.
/*   async function PostData() {  
    PostRequest();
    ToProductPage();
    }; */

   
    function PostData() {
    //Get data from the all inputboxes
    var sku = $("#newProductDiv")
      .find('input[name="sku"]')
      .val();

    var name = $("#newProductDiv")
      .find('input[name="name"]')
      .val();

    var price = $("#newProductDiv")
      .find('input[name="price"]')
      .val();

    var weight = $("#SwitcherBoxes")
      .find('input[name="weight"]')
      .val();

    var size = $("#SwitcherBoxes")
      .find('input[name="size"]')
      .val();

    var height = $("#SwitcherBoxes")
      .find('input[name="height"]')
      .val();

    var width = $("#SwitcherBoxes")
      .find('input[name="width"]')
      .val();

    var length = $("#SwitcherBoxes")
      .find('input[name = "length"]')
      .val();

    // To use dimensions in HxWxL format
    var dimensions = height + "x" + width + "x" + length;

    // Use given data post to the server
    // If switcher is (Book) then (size) and (dimensions) will be undefined to post correctly
    var obj = {
      sku: sku,
      name: name,
      price: price,
      weight: weight,
      size: size,
      dimensions: dimensions
    };

    // Make request for save the data

      var API = "http://46.101.195.9/server/index.php";
    /* var API = "http://localhost:8000/server/index.php";
    */

    axios
      .post(API, obj) 
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });

        /* // Returns to the “Product List” page with the new product added
  setTimeout(function(){ window.location.href="/" }, 3000); */
  };


  

  


