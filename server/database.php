<?php
/* header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
@ini_set('zlib.output_compression', 'Off');
@ini_set('implicit_flush', 1);
header("Content-Encoding: identity"); */


class Database
{
    private $servername = "localhost";
    private $username = "web";
    private $password = "2webP455!}";
    private $database = "task";
    public $conn;

/*     private $servername = "localhost";
    private $username = "id18674598_username_example";
    private $password = "6R8/KBMa&Iu#4#!/";
    private $database = "id18674598_example";
    public $conn; */
    

    
    public function getConnection()
    {
        $this->conn = null;
        
        try {
            $conn = new PDO("mysql:host=$this->servername;dbname=$this->database", 
                    $this->username, $this->password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Connected successfully"; 
        } catch(PDOException $e) {    
            echo "Connection failed: " . $e->getMessage();
            }

        return $this->conn;
    }
    
    public function executeAllQueries()
    {
        $this->getConnection();
        
        $con = mysqli_connect($this->servername,$this->username,$this->password,
                                $this->database);
    
        // Operate the getting data query (Select - get data, * - get from all columns data, FROM "Name of table")
        $query1 = "SELECT * FROM book";
        $query2 = "SELECT * FROM dvd";
        $query3 = "SELECT * FROM furniture";


        // Run Query 1
        $result = mysqli_query($con, $query1);

        // Create an array to store products
        $arr = array();

        // After execution of queries push them all into array
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            array_push($arr, array(
                'sku' => $row['SKU'],
                'name' => $row['NAME'],
                'price' => $row['PRICE'],
                'weight' => $row['WEIGHT']
            ));
        }


        // Run Query 2
        $result = mysqli_query($con, $query2);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            array_push($arr, array(
                'sku' => $row['SKU'],
                'name' => $row['NAME'],
                'price' => $row['PRICE'],
                'size' => $row['SIZE']
            ));
        }


        // Run Query 3
        $result = mysqli_query($con, $query3);


        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            array_push($arr, array(
                'sku' => $row['SKU'],
                'name' => $row['NAME'],
                'price' => $row['PRICE'],
                'dimensions' => $row['DIMENSIONS']
            ));
        }
        
        return $arr;
        
        // Close connection
        mysqli_close($con);

        
    }

    public function insertDataToTable($sku, $name, $price, $lastVal, 
                                        $attributeName, $tableName)
    {
       $this->getConnection();
        
       $con = mysqli_connect($this->servername,$this->username,
                                $this->password,$this->database);

       // Operate the inserting query
       $query = "INSERT INTO {$tableName} 
                 (sku, name, price, {$attributeName}) 
                    VALUES ('$sku', '$name', '$price', '$lastVal')";

        $result = mysqli_query($con, $query);

        mysqli_close($con);
        
    }

    public function deleteDataFromTable($sku, $tableName)
    {
        $this->getConnection();

        $con = mysqli_connect($this->servername,$this->username,
                                $this->password,$this->database);
        

            // Operate the deleting query
            // Prepare mysqli statements 
            $query = "DELETE FROM {$tableName} WHERE sku = '$sku'";
            print_r($query);
            $result = mysqli_query($con, $query);
            mysqli_close($con);
        
    }

    public function getRequest()
    {
        // Get data from the db table
        $myData = $this->executeAllQueries();

        // Request to get all data as a json
        echo json_encode($myData);
    }

    //To make request from another port
    public function enableCorsAttack()
    {
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }
        header("Content-Type: application/json; charset=UTF-8");

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                // may also be using PUT, PATCH, HEAD etc
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT");
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }

            exit(0);
        }
    }

}


//$tabula = new Database();
//$tabula->insertDataToTable("3","Good book","10","30","Weight","Book");
//$tabula->deleteDataFromTable("3","Book");
//$tabula->getRequest();
//$tabula->executeAllQueries(); - nestrādā un nevar arī print viņas vērtību 
?>


